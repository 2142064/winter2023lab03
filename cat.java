public class cat{
	public String color;
	public double weight;
	public String name;

	
	public void meows(String name){
			System.out.println(name+" meows loudly!");
	}
	
	public void inShape(double weight, String name){
		if(weight > 15){
			System.out.println(name+" is obese at the weight of: "+weight);
		}else if(weight < 10){
			System.out.println(name+" is underweight at the weight of: "+weight);
		}else{
			System.out.println(name+" is healthy at the weight of: "+weight);
		}
}
}